# Career Check-In Template

A set of questions to help facilitate career development check-ins. I recommend a recurring check-in cadence ranging from monthly to quarterly.

## First-time career discussion

1. Discuss and clarify short and long term goals. Ideally short term goals with align with longer term goals. For example, a long-term goal of becoming a technical leader may lead to short-term goals such as "work towards Staff engineer" or "contribute to an architecture blueprint".
1. Identify 2-3 opportunities/challenges that will help achieve the stated goals. The opportunities may consist of multiple mini-goals and these mini-goals should be SMART (specific, measurable, achievable, relevant, time-bound).

## Check-in

1. Review short and long-term goals. Have any of your goals changed? Any you want to add or remove?
1. For each in-progress growth opportunity, ask:
    1. What progress have you made on this opportunity since last check-in?
    1. What's the estimated completion percentage?
    1. Any updates to your plan?
    1. Is there anything we can utilize the [Growth & Development Benefit](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/) for to make progress?
1. Identify any new growth opportunities and define SMART goals to accomplishing them.
1. Are there any other career related topics you'd like to discuss?
