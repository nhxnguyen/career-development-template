# The Discovery Questionnaire

Taken from Coaching for Results LinkedIn Learning course with Lisa Gates. Use this questionnaire at the beginning of your coaching relationship and review this document every 6 to 12 months for updates.

## Assess Your Role

1. What do you love about your work?
1. What do you wish you could change?
1. If you were to review your current job description, what components do you no longer do? What new responsibilities have you taken on?

## Describe Your Achievements and Goals

1. What are your most satisfying achievements, not just in your current position but in your entire career?
1. What do you want to accomplish this year?
1. Where do you see yourself in the next five years?
1. What support and/or resources do you need to achieve your immediate goals? 5. What support and/or resources do you need to achieve your career aspirations? 1. What is your ideal work?
1. What skills do people acknowledge you for?
1. What skills or talents would you like to be acknowledged for?

## Identify Your Work Style

1. How much of a priority are you in your own life?
1. How well do you keep promises to yourself and others?
1. How satisfied are you with your level of productivity?
1. How well do you communicate with others?
1. What areas of your work life would you MOST like to improve? 6. What routinely gets in your way?

## Express Your Vision

1. If you were the CEO, where would you take this company?
1. If you could change the world (and you can), what needs would you meet or what problems would you solve?
1. What two steps could you take right now that would make the biggest difference in your life and work?
1. Anything else?
