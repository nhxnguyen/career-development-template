# Jon Snow's Career Development Plan

**_Clone this template and replace examples with your own heroic quest._**

**_Ensure the project is set to private and share with your manager and any other collaborators._**

## Long Term Goals

* Defeat the Whitewalkers

## Short Term Goals

* Defend Castle Black
* Find allies to fight the Whitewalkers
* Learn how to ride a dragon

## Strengths and Opportunities

### Strengths

#### Self-Assessment

#### Manager Comments

### Opportunities

#### Self-Assessment

#### Manager Comments

## Growth Opportunities Tracking

* [Opportunity 1](./opportunity_template.md)

## Accomplishments and Highlights

## Feedback

