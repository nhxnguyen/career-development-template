# Improve Sword Skills

## What to work on

* 30 minutes daily solo training
* Regular sparring with partners
* Study sword training videos on "Westeros Learn" platform

## Discussion Log

* yyyy-mm-dd Manager: You're making great progress! Pay attention to your footwork and stay off your heels when dodging.
* yyyy-mm-dd Jon: Was unable to practice for 2 weeks due to recovering from my injuries during the Night's Watch mutiny. 
